editCourse = async() => {
   
    let courseId = localStorage.getItem("courseId")
    const getCourse = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/${courseId}`)

    const data = await getCourse.json()

    let course = document.getElementById('courseName')
    let price = document.getElementById('coursePrice')
    let description = document.getElementById('courseDescription')


    course.value = data.courseName
    price.value = data.price
    description.value = data.description

    let token = localStorage.getItem("accessToken")

    const editForm = document.querySelector("#editCourse")

    editForm.addEventListener("submit", (e) => {
        e.preventDefault()

        update = async() => {

            //DECLARE IT AS AN OBJECT
            let newCourse = document.getElementById('courseName').value
            let newPrice = document.getElementById('coursePrice').value
            let newDescription = document.getElementById('courseDescription').value
            
            const updateObj = {
                "courseId": courseId,
                "courseName": newCourse,
                "price": newPrice,
                "description": newDescription
            }

           

           const result = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses`, {
               method: 'PUT',
               headers: { 'Content-type': 'application/json', 'Authorization': `Bearer ${token}` },
               body: JSON.stringify(updateObj)
           })

           const updatedData = await result.json()

           console.log(updatedData)

           if(updatedData !== '') {
               window.location.replace("./courses.html")
               alert(`Course has been updated`)
           } else {
               alert(`Error`)
           }

            //reset our localStorage
            localStorage.removeItem("courseId")
        }

        //Update 
        update()


        
    })
}

editCourse()