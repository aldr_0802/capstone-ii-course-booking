const registerForm = document.querySelector("#registerUser")
let errMsg = document.querySelector("#errMsg")

registerForm.addEventListener("submit", (e) => {
    e.preventDefault()

    const firstName = document.querySelector("#firstName").value
    const lastName = document.querySelector("#lastName").value
    const mobileNumber = document.querySelector("#phone").value
    const userEmail = document.querySelector("#email").value
    const password1 = document.querySelector("#password").value
    const password2 = document.querySelector("#verify-password").value


    if ((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)) {
      
        testEmail = async() => {
           const result = await fetch('https://whispering-brushlands-59338.herokuapp.com/api/users/email-exists', {
                method: 'POST',
                headers: {
                    'CONTENT-TYPE' : 'APPLICATION/JSON'
                },
                body: JSON.stringify({
                    "email": userEmail
                })
            })

            //return a promise that resolves when the result is loaded
            const data = await result.json()

            console.log(data)
            //If no duplicates found
            //Get the routes for registration in our server
        
            if (data == false) {

                const register = await fetch('https://whispering-brushlands-59338.herokuapp.com/api/users/', {
                    method: 'POST',
                    headers: {
                        'CONTENT-TYPE' : 'APPLICATION/JSON'
                    },
                    body: JSON.stringify({
                        "firstName": firstName,
                        "lastName": lastName,
                        "mobileNo": mobileNumber,
                        "email": userEmail,
                        "password": password1
                    })
                })

                const registerUser = await register.json()

                console.log(registerUser)
                window.location.replace("./login.html")
               
            } else {
               errMsg.innerHTML = `<div class='font-weight-bold'>* EMAIL ALREADY TAKEN</div>`
            }
            
        }

        //Invoke function
        testEmail()
    } else {
        errMsg.innerHTML = `<div class='font-weight-bold'>* PROVIDE REQUESTED INFO</div>`
    }
})