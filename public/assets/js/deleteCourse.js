archiveCourse = async() => {

    const courseId = localStorage.getItem("courseId")
    const token = localStorage.getItem("accessToken")
    const deleteURI = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/${courseId}`, {
        method: 'DELETE',
        headers: { 'Content-type': 'application/json', 'Authorization': `Bearer ${token}` }
    })

    const archived = await deleteURI.json()

 

    console.log(archived.isActive)
    
}

archiveCourse()

getAllArchived = async() => {
    const result = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/archives`, {
        method: 'GET',
        headers: { 'Content-type': 'Application/json' }
    })
    let courseData 
 

    const archived = await result.json()

    //Content of our containers
    courseData = archived.map(course => { return(
        `
        <div class="col-md-6 my-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">${course.courseName}</h5>
                    <p class="card-text text-left">
                        ${course.description}
                    </p>
                    <p class="card-text text-right">
                        ${course.price}
                    </p>
                </div>
            </div>
        </div>
        `
        )
    }).join("")

    let container = document.querySelector(".container")
    container.innerHTML = courseData

    window.location.replace("./courses.html")
}

getAllArchived()