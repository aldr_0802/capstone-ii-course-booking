let user = localStorage.getItem("isAdmin")

getCourse = async () => {
    let courseId = localStorage.getItem("courseId")
    const result = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/${courseId}`)

    const data = await result.json()

    let firstDiv = document.getElementById('courseName')
    firstDiv.innerHTML = data.courseName.toUpperCase() 

    let secDiv = document.getElementById('courseDesc')
    secDiv.innerHTML = data.description.toUpperCase() 
    
    let thirdDiv = document.querySelector("#coursePrice")
    thirdDiv.innerHTML = data.price

    $('#admin-table-of-studs').hide()

    //if regular user has logged in
 
    clickedButton = async () => {
        let enrolled = document.querySelector("#enroll")
        enrolled.addEventListener("click", (e) => {
    
            let token = localStorage.getItem("accessToken")
            let userId = localStorage.getItem("id")
            console.log(courseId)
    
            enroll = async () => {
                const enrollURL = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/enroll`, {
                    method: 'POST', 
                    headers: {
                        'Content-type': 'Application/JSON',
                        'Authorization': `bearer ${token}`
                    },
                    body: JSON.stringify({
                        "userId": userId,
                        "courseId": courseId
                    })
                })
    
                const data = await enrollURL.json()
    
                console.log(data)
    
                alert(`You have successfully enrolled at your desired course!`)
                window.location.replace("./profile.html")
            }
    
            enroll()
        })
    }

    let fourthDiv = document.getElementById('fourth-div')
    if(user == `false`) {
        fourthDiv.innerHTML = `
            <button class='btn btn-outline-dark mb-5 font-weight-bold' id='enroll'>Enroll Now!</button>
        `
        clickedButton()
    }
}

particularCourse = async() => {
    const uri = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/${localStorage.getItem("courseId")}`)

    const course = await uri.json()

    $('#lists').append(`LIST OF ENROLEES`)
    let firstDiv = document.getElementById('courseName')
    firstDiv.innerHTML = course.courseName

    let secDiv = document.getElementById('courseDesc')
    secDiv.innerHTML = course.description
    
    let thirdDiv = document.querySelector("#coursePrice")
    thirdDiv.innerHTML = course.price

    console.log(course)

        idsOfStud = async() => {
            const uri = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/spec/${localStorage.getItem("courseId")}`)

            const studs = await uri.json()

            console.log(studs)

            studs.forEach(i => {
                console.log(i)

                namesOfStud = async() => {

                    const data = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/${i.studentId}`)

                    const result = await data.json()

                    console.log(result)
            
                        $('#enrollContainer').append(
                            `
                            <tr>
                                <td>${result.firstName} ${result.lastName}</td>
                                <td><button id="drop-${result._id}" class="btn btn-outline-dark btn-block btn-sm">Change Status</button></td>
                                
                            </tr>
                            `
                        )

                    
                let dropStudBtn = document.querySelector(`#drop-${result._id}`)

                dropStudBtn.addEventListener("click", e => {
                    alert(result._id)
                    //courseId 
                    studentToDropped = async() => {
                        const url = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/drop`, {
                            method: 'POST',
                            headers: { 'Content-type': 'Application/JSON' },
                            body: JSON.stringify({
                                "_id": result._id, 
                                "courseId": localStorage.getItem("courseId")
                            })
                     })

                    const droppedStud = await url.json()

                    console.log(droppedStud)

                    alert(`STUDENT HAS BEEN DROPPED AND/OR ALLOWED TO THE COURSE`)
                   
                    }

                    studentToDropped()

                    
                })
                }

                namesOfStud()

            })

     
        }

        idsOfStud()
}

if(user == `false` || !user) {
    getCourse()
} else if(user == `true`) {
    console.log(`for admin`)
    particularCourse()
}

console.log(localStorage.getItem("courseId"))



//GET COURSE DETAILS
//GET STUDENTS ENROLLED


