
const loginForm = document.querySelector("#logInUser")
let errMsg = document.querySelector("#errMsg")

loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    const userEmail = document.querySelector("#email").value
    const password = document.querySelector("#password").value

    validateUser = async() => {
        const result = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/login`, {
            method: 'POST',
            headers: {
                'CONTENT-TYPE' : 'APPLICATION/JSON'
            },
            body: JSON.stringify({
                "email": userEmail,
                "password": password
            })
        })

        const data = await result.json()

        if (data !== false) {
            localStorage.setItem('accessToken', data.accessToken)

                const res = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })

                const userToken = await res.json()
                localStorage.setItem("id", userToken._id)
                localStorage.setItem("isAdmin", userToken.isAdmin)
                
                window.location.replace("./courses.html")
               
        } else {
            errMsg.innerHTML = `<div class='font-weight-bold mb-2'>* INVALID CREDENTIALS</div>`
        }
    }

    validateUser()
})
//RESEARCH ON HOW TO USE LOCALSTORAGE
//KEY AND VALUE

