activateCourse = async() => {
    let token = localStorage.getItem("accessToken")
    let courseId = localStorage.getItem("courseId")
    const activateURI = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/activate`, {
        method: 'POST',
        headers: { 'Content-type': 'application/json', 'Authorization': `Bearer ${token}` },
        body: JSON.stringify({"_id": courseId})
    })

    const result = await activateURI.json()

    console.log(result)

    
    //reset our localStorage
    localStorage.removeItem("courseId")
    
    alert("COURSE HAS BEEN ACTIVATED")
    window.location.replace("./courses.html")
}

activateCourse()