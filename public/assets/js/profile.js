userEnrollments = async() => {
  profile = async() => {
    const token = localStorage.getItem("accessToken")

      const readURI = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/`, {
          method: 'GET',
          headers: { Authorization: `Bearer ${token}` }
      })

      const result = await readURI.json()

      console.log(result)

      $('#fullName').append(`${result.firstName} ${result.lastName}`)
      $('#email').append(result.email)
      $('#phoneNo').append(result.mobileNo)
    }

    profile()

  const url = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/stat`, {
    method: 'POST',
    headers: { 'Content-type': 'Application/JSON' },
    body: JSON.stringify({
      "_id": localStorage.getItem("id")
    })
  })

  const result = await url.json()

  result.forEach(i => {
    console.log(i)

    fetchCourse = async() => {
      const fetching = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/${i.courseId}`)

      const course = await fetching.json()

      if(course._id == i.courseId) {
        console.log(course.courseName)

        $('#courses').append(

          `
            <tr>
                <td>${course.courseName}</td>
                <td>${i.enrolledOn}</td>
                <td>${i.status}</td>
            </tr>
          `
        )
      }
    }

    fetchCourse()
    })
}

let user = localStorage.getItem("isAdmin")
if (user == `false`) {
  userEnrollments()
} else {
    $('#class').hide()
    $('#listOfCourses').hide()
    console.log(`ADMIN`)
    userEnrollments()
}