let navItems = document.querySelector("#navSession")
let navItems1 = document.querySelector("#navSession1")
console.log(navItems)

let userToken = localStorage.getItem("accessToken")

console.log(userToken)

if(!userToken) {
    $('#3rd-link').hide()
    navItems1.innerHTML = 
    ` 
        <li class = "nav-item">
            <a href="./register.html" class="nav-link"> Register</a>
        </li>
    `
    navItems.innerHTML = `
        <li class = "nav-item">
            <a href="./login.html" class="nav-link">Log in</a>
        </li>
    `

} else {
   getUser = async() => {
    const getURL = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/users/`, {
        headers: {
            Authorization: `Bearer ${userToken}`
        }
    })

    const user = await getURL.json()

    console.log(user)
        navItems1.innerHTML = 
        ` 
        <li class = "nav-item">
            <a href="#" class="nav-link">${user.firstName} ${user.lastName}</a>
        </li>
        `
    

   }

   getUser()
    navItems.innerHTML = `
    <li class = "nav-item">
        <a href="./logout.html" class="nav-link">Log out</a>
    </li>

    `
}