let detectUser = localStorage.getItem("isAdmin")

let modalButton = document.querySelector("#adminButton")

console.log(detectUser)

showCourses = async() => {

    if(detectUser == `false` || !detectUser) {
        modalButton.innerHTML = null
        const selectCourse  = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/`)

        const result = await selectCourse.json()

        result.map(i => {
            $('#coursesContainer').append(
                `
                <div class="col-lg-6">
                    <div class="card mt-4 bg-dark">
                    <img class="card-img-top" src="https://media.giphy.com/media/dWesBcTLavkZuG35MI/source.gif" alt="Card image">
                        <div class="card-body">
                            <p class="courseTitle text-white"> ${i.courseName.toUpperCase() } </p>
                            <p class="courseDetails text-white">
                                ${i.description.toUpperCase()}
                            </p>
                            <p class="coursePrice text-right text-white"><i class="fas fa-pound-sign"></i> ${i.price} </p>
                            <div>
                                <button class="btn btn-outline-dark font-weight-bold" id="select-${i._id}"><i class="fa fa-eye" aria-hidden="true"></i> View Course</button>
                            </div>
                        </div>
                    </div>
                </div> 
                `
            )
     

            let enrollBtn = document.querySelector(`#select-${i._id}`)
            enrollBtn.addEventListener("click", (e) => {
                e.preventDefault()

                localStorage.setItem("courseId", i._id)
                window.location.replace("./course.html")
            })
        })
        $('#adminTable').hide()

    } else {
        modalButton.innerHTML =
        `
            <div class="col-md-2 offset-md-10 mb-2">
                <a href="./addCourse.html" class="btn btn-outline-dark btn-block btn-sm"><i class='fa fa-plus'></i> Add Course</a>
            </div>
        `

        const allCourses = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/archives`)

        const result = await allCourses.json()

        
        result.map(i => {
            $('#courseList').append(`<tr>`)
            $('#courseList').append(
                `
                    <td>${i.courseName.toUpperCase() }</td>
                    <td>${i.price}</td>
                    <td class="border-right">${i.description.toUpperCase() }</td>
                    <td>
                        <button class='btn btn-sm btn-outline-dark btn-block' id='view-${i._id}'><i class='fa fa-eye'></i> View </button>
                    </td>
                    <td>
                        <button class='btn btn-sm btn-outline-dark btn-block' id='edit-${i._id}'><i class='fa fa-file'></i> Edit </button>
                    </td>
                `
            )
            if(i.isActive == true) {
                $('#courseList').append(
                    `
                    <td>
                        <button class='btn btn-sm btn-outline-dark btn-block' id='archive-${i._id}'><i class='fa fa-trash'></i> Archive </button>
                    </td>
                    
                    `)

                    let archiveBtn = document.querySelector(`#archive-${i._id}`)
                    archiveBtn.addEventListener("click", (e) => {

                        localStorage.setItem("courseId", i._id)
                        window.location.replace('./deleteCourse.html')

                    })
            } else {
                $('#courseList').append(
                    `
                    <td>
                        <button class='btn btn-sm btn-outline-dark btn-block' id='activate-${i._id}'><i class='fa fa-check'></i> Activate </button>
                    </td>
                    
                `)

                let activateBtn = document.querySelector(`#activate-${i._id}`)
                activateBtn.addEventListener("click", (e) => {

                    localStorage.setItem("courseId", i._id)
                    window.location.replace('./activateCourse.html')
                })

            }
            $('#courseList').append(`</tr>`)

            let viewBtn = document.querySelector(`#view-${i._id}`)
            viewBtn.addEventListener("click", (e) => {
    
                    localStorage.setItem("courseId", `${i._id}`)
                    window.location.replace('./course.html')
    
             })

            let editBtn = document.querySelector(`#edit-${i._id}`)
            editBtn.addEventListener("click", (e) => {
    
                    localStorage.setItem("courseId", `${i._id}`)
                    window.location.replace('./editCourse.html')
    
             })

        })
    }
 
}

showCourses()





