const createCourse = document.querySelector("#createCourse")
createCourse.addEventListener("submit", (e) => {
    e.preventDefault()

    
    let courseName = document.querySelector("#courseName").value
    let coursePrice = document.querySelector("#coursePrice").value
    let courseDescription = document.querySelector("#courseDescription").value
    let eMsg = document.querySelector("#errorMsg")
    
    if(courseName == ''){
        eMsg.innerHTML = `<h6 class="alert alert-danger">* PROVIDE COURSE NAME</h6>`
    } else if(coursePrice == '') { 
        eMsg.innerHTML = `<h6 class="alert alert-danger">* PROVIDE COURSE FEE</h6>`
    } else if(courseDescription == ''){
        eMsg.innerHTML = `<h6 class="alert alert-danger">* PROVIDE COURSE DESCRIPTION</h6>`
    } else {

        let token = localStorage.getItem("accessToken")

        const saveObjects = {
            "courseName": courseName,
            "price": coursePrice,
            "description": courseDescription
        }


        verifyCourse = async() => {
            const verify = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/verify-course`, {
                method: 'POST',
                headers: { 'CONTENT-TYPE': 'APPLICATION/JSON',  'AUTHORIZATION': `Bearer ${token}` },
                body: JSON.stringify({
                    "courseName": courseName,
                    "description": courseDescription
                })
            })

            const isExisting = await verify.json()

            console.log(isExisting)

            if(isExisting !== false) {
                eMsg.innerHTML = `
                    <div class='alert alert-danger' role='alert'> 
                        <h4 class="alert-heading"> * SIMILAR COURSE DETAILS HAS BEEN ADDED TO OUR LIST </h4>
                        <hr>
                        <p> Please make sure to add new course information/details</p>
                    </div>`
            } else {
                addCourse = async() => {
                    const createURI = await fetch(`https://whispering-brushlands-59338.herokuapp.com/api/courses/`, {
                        method: 'POST',
                        headers: { 'CONTENT-TYPE': 'APPLICATION/JSON',  'AUTHORIZATION': `Bearer ${token}` },
                        body: JSON.stringify(saveObjects)
                    })
        
                    const data = await createURI.json()
        
                    if(data !== '') {
                        $('#createCourse').hide()
                        eMsg.innerHTML = `
                            <div class="alert alert-success" role="alert">
                               <h4 class="alert-heading"> * YOU HAVE SUCCESSFULLY CREATED A COURSE </h4>
                               Here are the details
                               <hr>
                               <p> The added course in the list is <strong> ${data.courseName} </strong> </p>
                               <p> It is offered at a price of <strong> ${data.price} </strong> </p>
                               <p> It focuses on teaching students about <strong> ${data.description} </strong> </p>
                               <hr>
                               <a class='text-primary' href='./courses.html'>RETURN</a>
                            </h6>
                        `
                    } else {
                        eMsg.innerHTML = `<div class='alert alert-danger'> SOMETHING WENT WRONG </div>`
                    }
                }
        
                addCourse()
            }
        }
        verifyCourse()

        
    }
})